# Dev tools

Collections of tools used in development, packed in a small VueJS application.

A [live version is available](https://dev-tools-experimentslabs-e815947742f338828e2b8e4c2c43eb61585ba.gitlab.io) for direct use.

**Community links:**
- [Code repository](https://gitlab.com/experimentslabs/dev-tools)
- [Issues](https://gitlab.com/experimentslabs/dev-tools/-/issues)
- [Community chat rooms](https://matrix.to/#/#experimentslabs:matrix.org) -
  Join [#general](https://matrix.to/#/!qpmxpfSIevwoRUgrxm:matrix.org?via=matrix.org) for questions and general
  discussions

**Other documentation:**
- [Code of conduct](CODE_OF_CONDUCT.md)
- [Contribution guide](CONTRIBUTING.md) - Contributions are welcome, but read this.
- [Changelog](CHANGELOG.md)

## Customize Vite configuration

See [Vite Configuration Reference](https://vitejs.dev/config/).

## Project setup

```sh
yarn install
```

### Compile and hot-reload for development

```sh
yarn run dev
```

### Compile and minify for production

```sh
yarn run build
```

## Tools

### Testing

There is no tests for now but if you want to write some, please use [vitest](https://vitest.dev/).

### Linters

**Javascript**: we use ESLint to lint javascript files:

```sh
# Display linting errors
yarn lint:js
# Automatically fix errors
yarn lint:js --fix 
```

**SCSS**: we use Stylelint to lint SCSS files:

```sh
# Display linting errors
yarn lint:style
# Automatically fix errors
yarn lint:style --fix
```

### CI

We use Gitlab CI for automated tests and the project is deployed
on [Gitlab Pages](https://dev-tools-experimentslabs-e815947742f338828e2b8e4c2c43eb61585ba.gitlab.io) for every new tag.

## Contributing

Contributions are very welcome : fixes, new features, etc... Please read the [Contribution guide](CONTRIBUTING.md) to
get started.
