# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

<!--
Quick remainder of the possible sections:
-----------------------------------------
### Added
  for new features.
### Changed
  for changes in existing functionality.
### Deprecated
  for soon-to-be removed features.
### Removed
  for now removed features.
### Fixed
  for any bug fixes.
### Security
  in case of vulnerabilities.
### Maintenance
  in case of rework, dependencies change

Please, keep them in this order when updating.
-->

## [Unreleased]

## [1.0.3] 2024/01/26 - Fixes

### Added

- Added a button to scroll to page top

### Fixed

HAR viewer:

- Handle JSON parsing errors in response content
- Improve filters with content type and response size
- Remove file type check and allow any file in the drop area


## [1.0.2] 2023/12/29 - Gitlab pages

### Added

- Configuration for Gitlab CI and Gitlab pages

### Fixed

- Use monospaced fonts in text areas
- **Har viewer**: Disable submit button when form is empty

## [1.0.1] 2023/12/16 - Fix bundle size

### Fixed

- CSS: RiseUI stylesheets is now used from the SCSS files instead of the bundled CSS 

## [1.0.0] 2023/12/16 - Initial release

First release with

- HAR viewer: Drag'n'drop or paste a HAR file and display its content
- GeoIP info display: Enter an IP and get its geoIP information
- .env matcher: Fills a `.env` with values from another
- Heroku rails log viewer: Paste your Rails logs from Heroku and group entries by IP
