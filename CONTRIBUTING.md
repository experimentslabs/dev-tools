<!-- omit in toc -->
# Contributing to Dev tools

First off, thanks for taking the time to contribute! ❤️

All types of contributions are encouraged and valued. See the [Table of Contents](#table-of-contents) for different ways
to help and details about how this project handles them. Please make sure to read the relevant section before making
your contribution. It will make it a lot easier for us maintainers and smooth out the experience for all involved. The
community looks forward to your contributions.

And if you like the project, but just don't have time to contribute, that's fine. Speaking about it to people that might
be interested is already a contribution :)

<!-- omit in toc -->
## Table of contents

- [Code of Conduct](#code-of-conduct)
- [I Have a Question](#i-have-a-question)
- [I Want To Contribute](#i-want-to-contribute)
- [Reporting Bugs](#reporting-bugs)
- [Suggesting Enhancements](#suggesting-enhancements)
- [Your First Code Contribution](#your-first-code-contribution)
- [Improving The Documentation](#improving-the-documentation)
- [Styleguides](#styleguides)
- [Commit Messages](#commit-messages)
- [Join The Project Team](#join-the-project-team)


## Code of conduct

This project and everyone participating in it is governed by the
[Code of Conduct](CODE_OF_CONDUCT.md).
By participating, you are expected to uphold this code. Please report unacceptable behavior to
`contact-project+experimentslabs-dev-tools-53115925-issue-@incoming.gitlab.com`. Received emails are not
public.


## I Have a question

> If you want to ask a question, we assume that you have read the available documentation listed in the
> [README](README.md).

Before you ask a question, it is best to search for
existing [Issues](https://gitlab.com/experimentslabs/dev-tools/-/issues) that might help you. In case
you have found a suitable issue and still need clarification, you can write your question in this issue. It is also
advisable to search the internet for answers first.

If you then still feel the need to ask a question and need clarification, we recommend the following:

- Open an [Issue](https://gitlab.com/experimentslabs/dev-tools/-/issues/new).
- Provide as much context as you can about what you're running into (try to fill the provided template).
- Provide project and platform versions (nodejs, npm, etc), depending on what seems relevant.

We will then take care of the issue as soon as possible.

You also can reach us on our Matrix chatrooms (listed in the [README](README.md))

## I want to contribute

> ### Legal notice <!-- omit in toc -->
> When contributing to this project, you must agree that you have authored 100% of the content, that you have the
> necessary rights to the content and that the content you contribute may be provided under the project license.

### Reporting bugs

#### Before submitting a bug report

A good bug report shouldn't leave others needing to chase you up for more information. Therefore, we ask you to
investigate carefully, collect information and describe the issue in detail in your report. Please complete the
following steps in advance to help us fix any potential bug as fast as possible.

- Make sure that you are using the latest version.
- Determine if your bug is really a bug and not an error on your side e.g. using incompatible environment
  components/versions (Make sure that you have read the documentation (check the [README](README.md) for a full list).
  If you are looking for support, you might want to check [this section](#i-have-a-question)).
- To see if other users have experienced (and potentially already solved) the same issue you are having, check if there
  is not already a bug report existing for your bug or error in
  the [bug tracker](https://gitlab.com/experimentslabs/dev-tools/-/issues/-/issues?label_name[]=Type%3A%3ABug).
- Also make sure to search the internet (including Stack Overflow) to see if users outside of the GitLab community have
  discussed the issue.
- Collect information about the bug:
  - Stack trace (Traceback)
  - OS, Platform and Version (Windows, Linux, macOS, x86, ARM)
  - Version of the interpreter, compiler, SDK, runtime environment, package manager, depending on what seems relevant.
  - Possibly your input and the output
  - Can you reliably reproduce the issue? And can you also reproduce it with older versions?

<!-- omit in toc -->
#### How do I submit a good bug report?

> You must never report security related issues, vulnerabilities or bugs including sensitive information to the issue
> tracker, or elsewhere in public.
> Instead sensitive bugs must be sent by email to
> contact-project+experimentslabs-dev-tools-53115925-issue-@incoming.gitlab.com.

We use GitLab issues to track bugs and errors. If you run into an issue with the project:

- Open an [Issue](https://gitlab.com/experimentslabs/dev-tools/-/issues/issues/new). (Since we can't be
  sure at this point whether it is a bug or not, we ask you not to talk about a bug yet and not to label the issue.)
- Explain the behavior you would expect and the actual behavior.
- Please provide as much context as possible and describe the *reproduction steps* that someone else can follow to
  recreate the issue on their own. This usually includes your code. For good bug reports you should isolate the problem
  and create a reduced test case.
- Provide the information you collected in the previous section, in the issue template.

Once it's filed:

- The project team will label the issue accordingly.
- A team member will try to reproduce the issue with your provided steps. If there are no reproduction steps or no
  obvious way to reproduce the issue, the team will ask you for those steps and mark the issue
  as `Status::Needs reproduction`. Bugs with the `Status::Needs reproduction` tag will not be addressed until they are
  reproduced.
- If the team is able to reproduce the issue, it will be marked `Status::Available`, as well as possibly other tags, and
  the issue will be left to be [implemented by someone](#your-first-code-contribution).

### Suggesting enhancements

This section guides you through submitting an enhancement suggestion for Dev tools, **including completely new
features and minor improvements to existing functionality**.
Following these guidelines will help maintainers and the community to understand your suggestion and find related
suggestions.

<!-- omit in toc -->
#### Before submitting an enhancement

- Make sure that you are using the latest version.
- Read the documentation (check the [README](README.md) for a full list) carefully and find out if the functionality is
  already covered, maybe by an individual configuration.
- Perform a [search](https://gitlab.com/experimentslabs/dev-tools/-/issues/issues) to see if the
  enhancement has already been suggested. If it has, add a comment to the existing issue instead of opening a new one.
- Find out whether your idea fits with the scope and aims of the project. It's up to you to make a strong case to
  convince the project's developers of the merits of this feature.

<!-- omit in toc -->
#### How do I submit a good enhancement suggestion?

Enhancement suggestions are tracked as [GitLab issues](https://gitlab.com/experimentslabs/dev-tools/-/issues).

- Use a **clear and descriptive title** for the issue to identify the suggestion. It still may be changed by maintainers
  if they think it's unclear.
- Provide a **step-by-step description of the suggested enhancement** in as many details as possible.
- **Describe the current behavior** and **explain which behavior you expected to see instead** and why. At this point
  you can also tell which alternatives do not work for you.
- You may want to **include screenshots and animated GIFs** which help you demonstrate the steps or point out the part
  which the suggestion is related to. You can use [this tool](https://www.cockos.com/licecap/) to record GIFs on macOS
  and Windows, and [this tool](https://github.com/colinkeenan/silentcast)
  or [this tool](https://github.com/GNOME/byzanz) on Linux.
- **Explain why this enhancement would be useful** to most Dev Tools users. You may also want to point out the other
  projects that solved it better and which could serve as inspiration.

Maintainers will review the suggestion add the appropriate tags to the ticket as soon as possible.

### Your first code contribution

To make a code contribution, you should follow these steps:

1. Fork the repository and clone it on your computer (or synchronize your local fork with the main repository).
1. Check the [README](README.md) to get started with the project on your machine
1. Create a branch from (`develop`) and make your fixes. We use
   the [conventional commits](https://www.conventionalcommits.org) specification here for our commit messages.
1. Make sure the tests suites and linters pass.
1. You updated the changelogs for non-documentation changes (`CHANGELOG.md`).
1. Push your work and open a merge request on the Dev Tools repository, explaining your changes. The merge request form
   contains templates with a few things to do/keep in mind.
1. Check for comments and/or changes requests until a maintainer merges the request.

Note that if you're not so comfortable with git, you can state it in the MR, and a maintainer _may_ rebase/cleanup the
commits.

### Improving the documentation

For simple changes, corrections and small improvements, open a merge request without an issue.

For bigger changes (as rework of the documentation, deletions, large additions),
[open an issue](https://gitlab.com/experimentslabs/dev-tools/-/issues/new) first, explaining what you
want to do or to see improved.

## Styleguides

### Commit Messages

This project follow the [conventional commits](https://www.conventionalcommits.org) specification for commit messages.

As a reminder, these _types_ are already used:

- `feat` - This commit introduces a new feature or enhancement.
- `fix` - This commit fixes something.
- `rework` - This commit does not add anything but makes the code better.
- `chore` - This commit is a bit boring: code linting, generating files, adding dependencies, splitting files,
  updating, ...
- `release` - This commit marks a release preparation: changes in the migration guide, changelogs, version bump, ...

We are less strict on the _scope_, except for a few of them:

- `node` - Related to javascript dependencies

Beside these two, look at the commit log if you need inspiration

### Code format

Check the [README](README.md) for a list of the tools used for linting Javascript and SCSS files.

### Markdown

We use no tool to enforce Markdown files format, but we try to follow these conventions:

- Lines are at most 120 characters long
- use one dash for emphasis `_text_`
- use double asterisk for bigger emphasis `**text**`

Everything else is pretty free for now.

## Join the project team

You want to be part of the maintainers? Great!

You will need to have a good general understanding of the project and code, or at least on a part of it.

Be a living part of the community by joining the chatrooms (see the [readme](README.md) for links), participating on
issues, and ideally have some merge requests... merged.


## Attribution
This guide is based on the **contributing-gen**. [Make your own](https://github.com/bttger/contributing-gen)!
