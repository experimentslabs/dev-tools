/**
 * Fetches details of an IP address on external service
 *
 * @param   {string}            ip - IP to fetch
 *
 * @returns {Promise<Response>}    Service response
 */
export async function getGeoIP (ip) {
  return fetch(`http://ip-api.com/json/${ip}?fields=status,message,country,countryCode,region,regionName,city,zip,isp,org,as`)
}
