import { createRouter, createWebHashHistory } from 'vue-router'
import HomeView from '../views/HomeView.vue'

const router = createRouter({
  history: createWebHashHistory(import.meta.env.BASE_URL),
  routes: [
    { path: '/', name: 'home', component: HomeView },
    { path: '/rails-logs', name: 'rails-logs', component: () => import('../views/RailsLogsView.vue') },
    { path: '/geo-ip', name: 'geo-ip', component: () => import('../views/GeoIPView.vue') },
    { path: '/env-matcher', name: 'env-matcher', component: () => import('../views/EnvMatcherView.vue') },
    { path: '/har-viewer', name: 'har-viewer', component: () => import('../views/HARViewer.vue') },
  ],
})

export default router
