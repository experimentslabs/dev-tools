export const menuLinks = {
  Dev: {
    '.env matcher': {
      url: { name: 'env-matcher' },
      description: 'Fills a .env file with another',
    },
    'HAR viewer': {
      url: { name: 'har-viewer' },
      description: 'Parse and renders HAR exports',
    },
  },
  Maintenance: {
    'Rails log analyzer': {
      url: { name: 'rails-logs' },
      description: 'Extracts IPS with multiple references in Rails logs from Heroku',
    },
    'GeoIP info': {
      url: { name: 'geo-ip' },
      description: 'Geolocate an IP',
    },
  },
}
