import { createApp } from 'vue'
import App from './App.vue'
import router from './router'

import { RiseUI } from '@experiments-labs/rise_ui'
import './stylesheets/styles.scss'

const app = createApp(App)

app.use(router)
  .use(RiseUI, {})

app.mount('#app')
